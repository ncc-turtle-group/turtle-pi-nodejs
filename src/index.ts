require('./fb-utils/firebase'); // Must be top level

import {CancellationToken} from "./types/CancellationToken";
import * as database from "./fb-utils/database"
import {DroneClient} from "./pi-utils/drone-client";

(async () => {
	try {
		// Init drone server connection
		const drone = DroneClient.getInstance();
		await drone.waitForCompleteInit();
		console.log('Connected to the drone');

		const cancellationToken = new CancellationToken();
		await database.enterMissionLoop(cancellationToken);
		process.exit();
	} catch (e) {
		console.log(e)
	}
})();


