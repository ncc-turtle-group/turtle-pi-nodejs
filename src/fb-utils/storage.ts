import firebase from "firebase-admin";
import {DroneClient} from "../pi-utils/drone-client";

const storage = firebase.storage();

const storageDir = 'image-uploads/';
const bucket = storage.bucket();

/**
 * Uploads provided image to Firebase storage.
 * */
export async function uploadImage(localImagePath: string, bucketImageName: string) {
	const drone = DroneClient.getInstance();
	console.log('Uploading ' + localImagePath);
	return await bucket.upload(localImagePath, {
		destination: storageDir + bucketImageName,
		metadata: {
			metadata: {
				location: await drone.getLocation()
			}
		}
	})
}