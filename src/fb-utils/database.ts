import firebase from "firebase-admin";
import {DroneClient, Location, MissionState} from "../pi-utils/drone-client";
import {CancellationToken} from "../types/CancellationToken";
import {sleep, uploadCapturedImage} from "../utils/utils";

// Get reference to the root of the database
const database = firebase.database();

// Get references
const activeMissionRef = database.ref('active-mission');
const piStatusRef = database.ref("pi-status");
const droneStatusRef = database.ref("drone-status");

const missionHistoryRef = firebase.firestore().collection('missionHistory');

const drone = DroneClient.getInstance();
let lastMissionState: MissionState = 'Pending';
let lastLogLength = 0;
let lastAborted = true;
let uploadIsEnabled = false

async function enterUploadLoop(loopToken: CancellationToken) {
	console.log('Starting Upload Loop')
	while (!loopToken.isCanceled) {
		await sleep(100)
		if (uploadIsEnabled)
			await uploadCapturedImage();
	}
}

export async function updateDroneStatus() {
	// Update drone status
	drone.getStatus().then(droneStatus => droneStatusRef.set(droneStatus));

	// Get mission status
	const missionStatus = await drone.getMissionStatus();

	// Get mission states and compare them
	const missionState = missionStatus['mission_state'];
	const missionLog = missionStatus['mission_log'];

	// Update mission logs if mission is executing
	if (missionState === 'Executing') {
		// Check if new logs have been added
		if (missionLog.length > lastLogLength) {
			activeMissionRef.child('mission_log').set(missionLog).then();
			if (missionLog[lastLogLength].startsWith('Reached waypoint')) {
				console.log(missionLog[lastLogLength]);

				// Enable upload once in the air
				if (missionLog[lastLogLength].startsWith('Reached waypoint 1'))
					uploadIsEnabled = true;
			} else if (missionLog[lastLogLength].startsWith('Returning to launch')) {
				uploadIsEnabled = false;
			}
			lastLogLength++;
		}
	}

	// Check if mission was aborted
	activeMissionRef.child('aborted').once('value').then(aborted => {
		if (aborted.val() && !lastAborted)
			drone.abortMission();
		lastAborted = aborted.val();
	});

	// Executing -> Finished transition
	if (missionState === 'Finished' && lastMissionState === 'Executing') {
		// Add current mission to mission history
		activeMissionRef.once('value').then(async snapshot => {
			const activeMission = snapshot.val();
			activeMission.finished_at = (new Date()).getTime();
			await activeMissionRef.child('mission_state').set('Finished');
			await missionHistoryRef.add(activeMission) // Upload to mission history
			await activeMissionRef.child('predicted-images').remove();
		});
	}

	lastMissionState = missionState;
}

export async function enterMissionLoop(loopToken: CancellationToken) {
	enterUploadLoop(loopToken).then()
	while (!loopToken.isCanceled) {
		// Update last online status of pi & drone
		await piStatusRef.child("last_online").set((new Date()).getTime());
		await updateDroneStatus();

		// Get active mission snapshot from database
		const activeMissionSnapshot = await activeMissionRef.once('value');

		if (activeMissionSnapshot.exists() &&
			activeMissionSnapshot.child('acknowledged').val() === false) {
			console.log('Started a mission');
			await activeMissionRef.child('acknowledged').set(true);
			await activeMissionRef.child('mission_state').set('Executing');
			await activeMissionRef.child('started_at').set((new Date()).getTime());
			lastLogLength = 0;

			// Get waypoints from the db & pass them to the drone
			const waypoints: Location[] = activeMissionSnapshot.child('waypoints').val();
			drone.startMission(waypoints).then();
		}
		await sleep(100);
	}
}