import firebase from "firebase-admin";

const serviceAccount = require("../../credentials.json");
firebase.initializeApp({
	credential: firebase.credential.cert(serviceAccount),
	databaseURL: "https://turtle-cloud.firebaseio.com/",
	storageBucket: 'turtle-cloud.appspot.com'
});