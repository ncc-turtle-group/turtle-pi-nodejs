import {captureImage} from "../pi-utils/camera";
import {uploadImage} from "../fb-utils/storage";

export const sleep = (ms: number) => new Promise(res => setTimeout(res, ms));

let isCapturing = false;
export async function uploadCapturedImage() {
	try{
		if (isCapturing)
			return;

		isCapturing = true;
		const captureInfo = await captureImage();
		isCapturing = false;
		await uploadImage(captureInfo.imagePath, captureInfo.imageName);
	} catch (e) {
		console.error('uploadCapturedImage: '+ e.message)
		isCapturing = false
	}
}