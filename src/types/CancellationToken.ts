export class CancellationToken {
	private _isCanceled = false;

	public get isCanceled() {
		return this._isCanceled;
	}

	public cancel() {
		this._isCanceled = true;
	}
}