// Singleton pattern implementation
import fetch, {FetchError, Response} from "node-fetch";

export type MissionState = 'Pending' | 'Executing' | 'Finished'

export interface Location {
	latitude: number
	longitude: number
	altitude: number
}

export interface DroneStatus {
	state: string
	mode: string
	battery: string
	armed: string
	airspeed: string
	next_command: string
	location: Location
}


export class DroneClient {
	private static instance: DroneClient;

	// private flaskServer: ChildProcess;
	private readonly flaskServerURL = 'http://localhost:5000';

	private constructor() {
		/*
		const scriptPath = `${require('os').homedir()}/drone-py/src/drone_server.py`; //TODO: Parameterize

		// Start the process
		this.flaskServer = spawn('python3', [scriptPath]);

		// Attach error listener
		this.flaskServer.stderr.on('error', chunk => {
			// TODO: Filter out non-error messages
			console.error('Flask Server Error:\n' + chunk.toString())
		});

		*/
	}

	public static getInstance(): DroneClient {
		if (!DroneClient.instance)
			DroneClient.instance = new DroneClient();

		return DroneClient.instance;
	}

	/**
	 * Since it takes couple of seconds to spin up the flask server,
	 * it is important to wait until it is completely turned on.
	 * This function helps with that. Simply `await` it.
	 * */
	public async waitForCompleteInit() {
		while (true) {
			try {
				await fetch(`${this.flaskServerURL}/init`, {method: 'GET'});
				break
			} catch (err) {
				if (!(err instanceof FetchError))
					console.error(err.message)
			}
		}
	}

	/**
	 * Returns fetch promise, hence needs to be awaited
	 * @param request
	 * @param method
	 * @param headers
	 * @param data
	 */
	private async query(request: string,
	                    method = 'GET',
	                    headers: any = undefined,
	                    data: any = undefined): Promise<Response> {
		try {
			return fetch(`${this.flaskServerURL}/${request}`, {
				method: method,
				headers: headers,
				body: JSON.stringify(data)
			});
		} catch (error) {
			if (error instanceof FetchError)
				console.log('no response');
			console.error(error);
			return error.message
		}
	}

	public async abortMission() {
		return (await this.query('abort_mission')).text()
	}

	public async getLocation() {
		return (await this.query('get_location')).text()
	}

	public async getStatus(): Promise<DroneStatus> {
		return (await this.query('get_status')).json()
	}

	public async getMissionStatus() {
		return (await this.query('get_mission_status')).json()
	}

	public async getMissionLog(): Promise<string[]> {
		return (await this.query('get_mission_log')).json()
	}


	public async getMissionState(): Promise<MissionState> {
		return (await this.query('get_mission_state')).text() as Promise<MissionState>
	}

	public async startMission(waypoints: Location[]) {
		await this.query(
			'start_mission',
			'POST',
			{'Content-Type': 'application/json'},
			waypoints)
	}

	/*
	public end() {
		this.flaskServer.kill('SIGINT')
	}
	*/
}

