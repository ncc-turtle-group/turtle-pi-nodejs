import {StillCamera, StillOptions} from "pi-camera-connect";
import * as fs from "fs";
import {getDateTime} from "../utils/time-utils";

export class CaptureInfo {
	constructor(readonly imageName: string, readonly imagePath: string) {
	}
}

// Take still image and save to disk
export async function captureImage() {
	const imageDir = '/home/dietpi/DCIM/'; //TODO: Extract to config
	const imageName = getDateTime() + '.jpg';
	const imagePath = imageDir + imageName;

	// console.info('Capturing image to: ' + imagePath);

	const hdConfig = {
		width: 1280,
		height: 720
	} as StillOptions;

	const fhdConfig = {
		width: 1920,
		height: 1080
	};

	const stillCamera = new StillCamera(hdConfig);

	const image = await stillCamera.takeImage();

	fs.writeFileSync(imagePath, image);

	return new CaptureInfo(imageName, imagePath)
}

